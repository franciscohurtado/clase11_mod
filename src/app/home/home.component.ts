import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  memoria: string = "";

  constructor(public router: Router) { }

  ngOnInit() {
  }

  interceptar(e) {
    this.memoria = e;
  }

  navigateTo(route){
    this.router.navigate([route]);
  }
}
