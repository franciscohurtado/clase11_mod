import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { API, API_KEY } from '../providers';

interface Agencia {
  name: String,
  short_name: String,
  phone: Number,
  language: String,
  agency_id: Number,
  long_name: String,
  bounding_box: Array<Object>,
  url: String,
  timezone: String,
  position: Object
}

@Injectable({
  providedIn: 'root'
})
export class MapaService {
  private api = API;
  private key = API_KEY;
  private service = "agencies.json";
  private serviceStops = "/stops.json?agencies="
  //private service = "agencies.json?agencies=12";

  public agencia: Agencia = {
    name: null,
    short_name: null,
    phone: null,
    language: null,
    agency_id: null,
    long_name: null,
    bounding_box: null,
    url: null,
    timezone: null,
    position: null

  }


  constructor(private http: HttpClient) { }


  get() {
    let headers = new HttpHeaders();
    headers = headers.set('X-Mashape-Key', `${this.key}`);
    let uri = `${this.api}${this.service}`;

    return this.http.get(uri, { headers: headers });

  }

  getStops(id) {
    let headers = new HttpHeaders();
    headers = headers.set('X-Mashape-Key', `${this.key}`);
    let uri = `${this.api}${this.serviceStops}${id}`;

    return this.http.get(uri, { headers: headers });

  }
}
