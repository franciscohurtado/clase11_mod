import { Component, OnInit, Renderer, ElementRef } from '@angular/core';
import { MapaService } from '../providers/providers';
import { Router } from '@angular/router';
import {NullAttrPipe} from '../pipes/null-attr.pipe';
import * as L from 'leaflet';


@Component({
  selector: 'app-mapa',
  templateUrl: './mapa.component.html',
  styleUrls: ['./mapa.component.scss']
})
export class MapaComponent implements OnInit {

  mapa: any;
  geopoint:any;
  markers:any;
  pipeAttr = new NullAttrPipe();

  constructor(public mapaService: MapaService, private router: Router,elementRef: ElementRef, renderer: Renderer) { 
    // Listen to click events in the component

    renderer.listen(elementRef.nativeElement, 'click', (event) => {
      console.log(event.target.classList.contains('buscar'));
        if(event.target.classList.contains('buscar')) {
          let _id = event.target.dataset.id;
           this.buscarStop(_id);
        }
    });
  }

   map :any;
   myIcon = L.icon({
    iconUrl: './assets/leaflet/images/marker-icon.png',
   
    shadowUrl: './assets/leaflet/images/marker-shadow.png'

    
   
});

  ngOnInit() {
    this.map = L.map('map').setView([51.505, -0.09],4);
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(this.map);
    
    this.buscarAgencias();


    
  }


  ngAfterViewInit() {
    
  }

  buscarAgencias(){
    let marker = [];

    this.mapaService.get().subscribe(mapa => {
      this.mapa = mapa;
        console.log(this.mapa.data);
        let agencias = this.mapa.data
        for (const agencia in agencias) {
          if (agencias.hasOwnProperty(agencia)) {
            const element = agencias[agencia];
            let attrUrl = this.pipeAttr.transform(element.url);
            marker.push(L.marker([element.position.lat, element.position.lng], {icon: this.myIcon})
            .bindPopup(`nombre: ${element.long_name} <br>
                        web: ${attrUrl} 
                        <button class='buscar' data-id='${element.agency_id}' >buscar</button>`)
            .openPopup());
            this.map.panTo(new L.LatLng(element.position.lat, element.position.lng));
  
          }
        }
        this.markers = L.layerGroup(marker);
  
        this.map.addLayer(this.markers);
  
  
      });
  }


  buscarStop(id){
    console.log(id);
    this.map.removeLayer(this.markers);
    let marker = [];
    this.mapaService.getStops(id).subscribe(agenciasStops => {
      this.mapa = agenciasStops;
        console.log(this.mapa.data);
        let agenciasS = this.mapa.data
        for (const agencia in agenciasS) {
          if (agenciasS.hasOwnProperty(agencia)) {
            const element = agenciasS[agencia];
            marker.push(L.marker([element.location.lat, element.location.lng], {icon: this.myIcon})
            .bindPopup(`parada`)
            .openPopup());
            this.map.panTo(new L.LatLng(element.location.lat, element.location.lng));

          }
        }
        this.markers = L.layerGroup(marker);

        this.map.addLayer(this.markers);

        // this.map.addLayer(this.marker);
      });
  }

}
