import { Directive, ViewContainerRef } from '@angular/core';
@Directive({
  selector: '[appAdMapa]'
})
export class AdMapaDirective {

  constructor(public viewContainerRef: ViewContainerRef) { }

}
